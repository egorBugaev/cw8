import React,{Component} from 'react'
import './Add.css'
import axios from 'axios'
class Add extends Component {
	state = {
		title: '',
		text: ''
	};
	
	changeTextHandler = (event) => {
		let text = this.state.text;
		text = event.target.value;
		this.setState({text});
	};
	
	changeTitleHandler = (event) => {
		let title = this.state.title;
		title = event.target.value;
		this.setState({title});
	};
	
	savePostHandler = () => {
		const post = {
			title: this.state.title,
			text: this.state.text,
			date: new Date(),
			disabled: true
		};
		axios.post('/posts.json', post).finally(() => {
			this.setState(prevState => {
				return {
					title: prevState.title = '',
					text: prevState.text = ''
				}
			});
			this.props.history.replace('/posts');
		});
	};
	
	render(){
		return (
			<div className="Add">
				<div className="Add__title">Add new post</div>
				<div className="Add__row">
					<label htmlFor="title">Title</label>
					<input onChange={(event) => this.changeTitleHandler(event)} value={this.state.title} id="title" type="text"/>
				</div>
				<div className="Add__row">
					<label htmlFor="text">Description</label>
					<textarea onChange={(event)=> this.changeTextHandler(event)} value={this.state.text} id="text"/>
				</div>
				<button onClick={this.savePostHandler} className="Add__save">SAVE</button>
			</div>
		)
	}
}

export default Add;