import React, {Component} from 'react'
import axios from 'axios'
import Spinner from "../../components/UI/Spinner/Spinner";
import Quotes from "../../components/Quotes/Quotes";


class star extends Component {

  state = {
    quotes: [],
    loading: false
  };

  updatePosts = async () => {
    this.setState({loading: true});
    let posts = await axios.get('/quotes.json?orderBy="category"&equalTo="star-wars"').finally(() => {
      this.setState({loading: false});
    });
    console.log(posts);
    let newPosts = [];
    for (let key in posts.data) {
      newPosts.push({
        author: posts.data[key].author,
        id: key,
        quote: posts.data[key].quote,
        category:posts.data[key].category,
        disabled: posts.data[key].disabled
      });
    }
    this.setState({quotes: newPosts});
  };

  componentDidMount() {
    this.updatePosts();
  }

  removeHandler = (id) => {
    const quotes = [...this.state.quotes];
    const index = quotes.findIndex(p => p.id === id);
    quotes.splice(index,1);
    axios.delete(`/quotes/${id}.json`).finally(() => {
      this.setState({quotes});
      this.props.history.replace('/quotes');
    });
  };


  edit = (id) => {
    let posts = [...this.state.quotes];
    const index = posts.findIndex(p => p.id === id);
    const queryParams = [];
    for(let key in this.state.quotes[index]) {
      queryParams.push(
        encodeURIComponent(key) + '=' + encodeURIComponent(this.state.quotes[index][key])
      );
    }
    const queryString = queryParams.join('&');
    this.props.history.push({
      pathname: '/quotes/edit:id',
      search: '?' + queryString
    })
  };

  render() {
    return (
      <div className='quotes'>
        {this.state.loading ? <Spinner/> : <Quotes edit={this.edit} remove={this.removeHandler}	quotes={this.state.quotes}/>}
      </div>
    )
  }
}

export default star;