import React from 'react'
import './Post.css'
const Post = props => {
	return (
		<li className="post">
			<div className="post__title">{props.title}</div>
			<div className="post__text">{props.text}</div>
			<button onClick={props.readMore} className="post__read-more">Read more</button>
		</li>
	)
};

export default Post;